### front-end-test

Esta é uma avaliação básica de código.

O objetivo é conhecer um pouco do seu conhecimento/prátido de consulta à API e tratamento das informações.

Recomendamos que você não gaste mais do que 3-4 horas para realizar este teste.

### Tarefas

- [ ] Faça um fork deste projeto. Ao finalizar faça um Merge Request da sua solução e nosso time será notificado.

- [ ] Buscar o json, no endereço abaixo, que lista as moedas cadastradas no sistema
```https://ichello.nyc3.digitaloceanspaces.com/test/currencies.json```

- [ ] Com a lista de moedas, mostrar na tela um card com o saldo total em USD do usuário (somar o balance de cada moeda e multiplicar pelo conversion.usd)

- [ ] Abaixo do total, criar uma tabela de todas as moedas retornada. Na tabela, mostrar as colunas: name, description, balance (utilizando a quantidade de casas decimais de acordo com o campo `decimals` de cada moeda - ex.: campo `decimals: 5`, `balance: 2.900139`, mostrar: `2.90014`), USD Balance (saldo em USD - multiplicar saldo pela conversão), BTC Balance

- [ ] Criar filtro na tabela acima para mostrar apenas as moedas que o usuário tem saldo (balance > 0).

### Dicas
- Projetos enviado usando vuejs serão um diferencial
- Você pode usar qualquer framework CSS ou CSS puro mas os projetos utilizando tailwind serão um diferencial
- As instruções para rodar o projeto devem ser claras no readme
